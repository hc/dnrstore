-module(dnrstore_mnesia_srv).
-include("dnrstore.hrl").
-include_lib("dnrstore/include/dnrstore.hrl").
-behaviour(gen_server).
-define(SERVER, ?DB_BACKEND).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init(Args) ->
  mnesia:start(),
  mnesia:create_table(user, [
    {attributes, record_info(fields, user)},
    {disc_only_copies,[node()]},
    {index, [#user.email]}
  ]),
  mnesia:create_table(domain, [
    {attributes, record_info(fields, domain)},
    {disc_only_copies,[node()]},
    {index, [#domain.user_id]}
  ]),
  mnesia:create_table(record, [
    {attributes, record_info(fields, record)},
    {disc_copies,[node()]},
    {index, [#record.domain_name]}
  ]),
  {ok, Args}.

handle_call({?DB_CREATE, Row}, _From, State) ->
  F = fun() -> mnesia:write(Row) end,
  {atomic, ok} = mnesia:transaction(F),
  {reply, ok, State};
handle_call({?DB_UPDATE, Row}, _From, State) ->
  handle_call({?DB_CREATE, Row}, _From, State);
handle_call({?DB_DELETE, Oid}, _From, State) ->
  F = fun() -> mnesia:delete(Oid) end,
  {atomic, ok} = mnesia:transaction(F),
  {reply, ok, State};
handle_call({?DB_QUERY, Q}, _From, State) ->
  F = fun() -> mnesia:match_object(Q) end,
  {atomic, Matches} = mnesia:transaction(F),
  {reply, {ok, Matches}, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

