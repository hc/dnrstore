-module(dnrstore_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    {ok, DbModule} = application:get_env(db_backend),
    dnrstore_sup:start_link([DbModule]).

stop(_State) ->
    ok.
