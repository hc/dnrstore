%% Copyright
-module(dnrstore).
-include("dnrstore.hrl").
-include_lib("dnrstore/include/dnrstore.hrl").

%% API
-export([
  start/0, stop/0,
  create_user/2, update_user/4, delete_user/1, find_users/1, find_users/2,
  create_domain/2, update_domain/4, delete_domain/1, find_domains/1,
  create_record/6, update_record/7, delete_record/1, find_records/1
]).

start() ->
  application:start(dnrstore).

stop() ->
  application:stop(dnrstore).

create_user(Email, Password) ->
  case find_users(Email) of
    [] -> ok;
    _ -> error("Email address already exists: " ++ Email)
  end,
  Id = generate_id(),
  Row = #user{
    id=Id,
    email=Email,
    password_hash=erlang:md5(Password)},
  ok = gen_server:call(?DB_BACKEND, {?DB_CREATE, Row}),
  {ok, Id}.

update_user(Id, Email, Password, EppKey) ->
  case find_user(Id) of
    [_User] -> ok;
    [] -> error("User does not exist.")
  end,
  Row = #user{
    id=Id,
    email=Email,
    password_hash=erlang:md5(Password),
    epp_key=EppKey},
  ok = gen_server:call(?DB_BACKEND, {?DB_UPDATE, Row}).

delete_user(Id) ->
  case find_user(Id) of
    [_User] -> ok;
    [] -> error("User does not exist.")
  end,
  Oid = {user, Id},
  ok = gen_server:call(?DB_BACKEND, {?DB_DELETE, Oid}).

find_user(Id) ->
  R = empty_user_record(),
  {ok, Response} = gen_server:call(?DB_BACKEND, {?DB_QUERY, R#user{
    id=Id
  }}),
  Response.

find_users(Email) ->
  R = empty_user_record(),
  {ok, Response} = gen_server:call(?DB_BACKEND, {?DB_QUERY, R#user{
    email=Email
  }}),
  Response.

find_users(Email, Password) ->
  R = empty_user_record(),
  {ok, Response} = gen_server:call(?DB_BACKEND, {?DB_QUERY, R#user{
    email=Email,
    password_hash=erlang:md5(Password)
  }}),
  Response.

create_domain(Name, UserId) ->
  case find_domain(Name) of
    [] -> ok;
    _ -> error("Domain already exists: " ++ Name)
  end,
  Row = #domain{
    name=Name,
    user_id=UserId,
    serial=generate_serial(),
    status=?STATUS_NEW},
  ok = gen_server:call(?DB_BACKEND, {?DB_CREATE, Row}).

update_domain(Name, UserId, Status, EppKey) ->
  case find_domain(Name) of
    [_Domain] -> ok;
    _ -> error("Domain does not exists: " ++ Name)
  end,
  Row = #domain{
    name=Name,
    user_id=UserId,
    serial=generate_serial(),
    status=Status,
    epp_key=EppKey},
  ok = gen_server:call(?DB_BACKEND, {?DB_UPDATE, Row}).

delete_domain(Name) ->
  case find_domain(Name) of
    [_Domain] -> ok;
    _ -> error("Domain does not exists: " ++ Name)
  end,
  case find_records(Name) of
    [] -> ok;
    _ -> error("Reference Error: Domain records exists for " ++ Name)
  end,
  Oid = {domain, Name},
  ok = gen_server:call(?DB_BACKEND, {?DB_DELETE, Oid}).

find_domain(Name) ->
  R = empty_domain_record(),
  {ok, Response} = gen_server:call(?DB_BACKEND, {?DB_QUERY, R#domain{
    name=Name
  }}),
  Response.

find_domains(UserId) ->
  R = empty_domain_record(),
  {ok, Response} = gen_server:call(?DB_BACKEND, {?DB_QUERY, R#domain{
    user_id=UserId
  }}),
  Response.

create_record(DomainName, Host, Type, Value, Ttl, Priority) ->
  case find_domain(DomainName) of
    [_Domain] -> ok;
    _ -> error("Cannot create record for non-existent domain: " ++ DomainName)
  end,
  Id = generate_id(),
  Row = #record{
    id=Id,
    domain_name=DomainName,
    host=Host,
    type=Type,
    value=Value,
    ttl=Ttl,
    priority=Priority},
  ok = gen_server:call(?DB_BACKEND, {?DB_CREATE, Row}),
  {ok, Id}.

update_record(Id, DomainName, Host, Type, Value, Ttl, Priority) ->
  case find_record(Id) of
    [_Record] -> ok;
    [] -> error("Record does not exist.")
  end,
  Row = #record{
    id=Id,
    domain_name=DomainName,
    host=Host,
    type=Type,
    value=Value,
    ttl=Ttl,
    priority=Priority},
  ok = gen_server:call(?DB_BACKEND, {?DB_UPDATE, Row}).

delete_record(Id) ->
  case find_record(Id) of
    [_Record] -> ok;
    [] -> error("Record does not exist.")
  end,
  Oid = {record, Id},
  ok = gen_server:call(?DB_BACKEND, {?DB_DELETE, Oid}).

find_record(Id) ->
  R = empty_record_record(),
  {ok, Response} = gen_server:call(?DB_BACKEND, {?DB_QUERY, R#record{
    id=Id
  }}),
  Response.

find_records(DomainName) ->
  R = empty_record_record(),
  {ok, Response} = gen_server:call(?DB_BACKEND, {?DB_QUERY, R#record{
    domain_name=DomainName
  }}),
  Response.


%% Helper

generate_id() ->
  erlang:md5(io_lib:format("~w~w", [node(), now()])).

generate_serial() ->
  io_lib:format("~.B~.B~.B", tuple_to_list(now())).

empty_user_record() ->
  list_to_tuple([user] ++ ['_' || _X <- record_info(fields, user)]).

empty_domain_record() ->
  list_to_tuple([domain] ++ ['_' || _X <- record_info(fields, domain)]).

empty_record_record() ->
  list_to_tuple([record] ++ ['_' || _X <- record_info(fields, record)]).


