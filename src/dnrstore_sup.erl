
-module(dnrstore_sup).
-include("dnrstore.hrl").

-behaviour(supervisor).

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link(Opts) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, Opts).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([DbModule]) ->
    {ok, {
      {one_for_one, 5, 10},
      [
        {?DB_BACKEND,
          {DbModule, start_link, []},
          permanent, 5000, worker, [DbModule]
        }
      ]
    }}.

