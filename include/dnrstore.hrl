%% Copyright
-author("hcvst").

-record(user, {
  id,
  email,
  password_hash,
  epp_key
}).

-record(domain,{
  name,
  user_id,
  serial,
  status,
  epp_key
}).

-record(record, {
  id,
  domain_name,
  host,
  type,
  value,
  ttl,
  priority
}).

